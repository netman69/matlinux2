#!/usr/bin/env qjs
import * as os from "os";
import * as std from "std";

var byte = (n) => String.fromCharCode(n & 0xFF);
var word = (n) => String.fromCharCode(n & 0xFF, (n >> 8) & 0xFF);
var dword = (n) => String.fromCharCode(n & 0xFF, (n >> 8) & 0xFF,
                                       (n >> 16) & 0xFF, (n >> 24) & 0xFF);
var rep = (n, c = 0) => String.fromCharCode.apply(this, Array(n).fill(c));
var err = (msg) => { throw msg; }

String.prototype.getBuf = function () {
	var buf = new ArrayBuffer(this.length);
	var bv = new Uint8Array(buf);
	for (var i = 0; i < this.length; i++)
		bv[i] = this.charCodeAt(i);
	return buf;
};

function open(fn, flags) {
	var fd = os.open(fn, flags) || err(`can't open file ${fn}`);
	var fl = (flags & os.O_RDWR || flags & os.O_WRONLY) ? "r+" : "r";
	return std.fdopen(fd, fl) || err(`can't open fd for file ${fn}`);
}

function write(f, buf) {
	if (f.write(buf, 0, buf.byteLength) != buf.byteLength)
		err("write failed");
}

function FATFS(fn, opts) {
	this.sectors = opts.sectors;
	this.sector_sz = opts.sector_sz || 512;
	this.cluster_sz = opts.cluster_sz || 1; // In sectors, 1/2/4/8/.../128.
	this.cluster_bsz = this.cluster_sz * this.sector_sz; // In bytes.
	this.fat_copies = opts.fat_copies || 2;
	this.align = opts.align || this.cluster_sz;
	this.reserve_extra = opts.reserve_extra || 0;
	this.root_sz = opts.root_sz || null; // Root dir entries, null = auto.
	this.offset = (opts.offset || 0) * this.sector_sz; // Offset entire FS.
	this.fsinfo_sector = 1; // Sector where FAT32 fsinfo is stored.
	this.boot_backup = 6; // Location of backup boot sector for FAT32.
	this.fat = [ 0x0FFFFF00 | 0xF8, 0x0FFFFFFF ];
	this.clusters_next = 2;
	var aligned = (n) => n + n % this.align;

	// Figure out minimum sectors per fat, etc. If total amount of clusters
	//   is within range return given and found parameters.
	var getspf = (reserved, root_sz, esz, type, clmax, min, max) => {
		var rs = Math.ceil(root_sz * 32 / this.sector_sz);
		var rem = this.sectors - aligned(reserved + this.reserve_extra) - rs;
		var eps = this.sector_sz / esz; // Entries per FAT sector.
		var spf = aligned(Math.ceil(rem / this.cluster_sz / eps));
		while (rem - spf * this.fat_copies <= spf * eps * this.cluster_sz)
			spf -= this.align;
		spf += this.align;
		rem -= spf * this.fat_copies;
		var clusters = Math.floor(rem / this.cluster_sz)
		if (clusters < min || clusters >= max)
			return null;
		return { // Merged with this-object later.
			type: type, // 12/16/32
			reserved: aligned(reserved + this.reserve_extra),
			fat_sz: spf, // In sectors.
			root_sz: root_sz, // Number of entries.
			clusters: clusters,
			clusters_free: clusters,
			cluster_max: clmax,
			start_off: this.sectors - rem
		};
	};

	// Figure out whether we should use FAT12/16/32.
	// The way the OS detects this is by the amount of clusters, so we only 
	//   get a choice by manipulating other parameters.
	Object.assign(this,
		getspf(1, this.root_sz || 224, 1.5, 12, 0xFF7, 0, 4085) ||
		getspf(1, this.root_sz || 512, 2, 16, 0xFFEE, 4085, 65525) ||
		getspf(32, 0, 4, 32, 0x0FFFFFF7, 65525, 0x0FFFFFF8) ||
		err("can't determine valid FAT type for given parameters")
	);

	// Applies the values from getspf() to this-object.
	this.root = new FATDir({ fs: this }, null, { size: this.cluster_bsz });
	this.root_start = this.root.cluster;

	// Open the output file.
	this.f = open(fn, os.O_RDWR | os.O_CREAT | (opts.notrunc ? 0 : os.O_TRUNC));

	// Write boot sector.
	var bs = "\xF4\xF4\xF4MSWIN4.1";
	bs += word(this.sector_sz);
	bs += byte(this.cluster_sz);
	bs += word(this.reserved);
	bs += byte(this.fat_copies);
	bs += word(this.root_sz);
	bs += (this.sectors < 0x10000) ? word(this.sectors) : word(0);
	bs += "\xF8"; // Media type.
	bs += (this.type != 32) ? word(this.fat_sz) : word(0);
	bs += word(1); // Sectors per track.
	bs += word(1); // Number of heads.
	bs += dword(0); // Number of hidden sectors.
	bs += (this.sectors < 0x10000) ? dword(0) : dword(this.sectors);
	if (this.type == 32) {
		bs += dword(this.fat_sz);
		bs += word(1 << 7); // Mirror flags.
		bs += word(0); // FAT32 version.
		bs += dword(this.root_start); // First cluster of root dir.
		bs += word(this.fsinfo_sector); // Sector where fsinfo stored.
		bs += word(this.boot_backup); // Backup boot sector.
		bs += rep(12);
	}
	bs += "\x80\x00\x29";
	bs += "\x0B\xAD\xC0\xDE";
	bs += `NO NAME    FAT${this.type}   `; // TODO FAT32 should just be "FAT     " i think
	bs += rep(510 - bs.length);
	bs += "\x55\xAA";
	this.sseek(0);
	write(this.f, bs.getBuf());
	if (this.type == 32 && this.boot_backup) {
		this.sseek(this.boot_backup);
		write(this.f, bs.getBuf());
	}

	// Extend the file to be big enough.
	this.sseek(this.sectors, -1);
	write(this.f, "\0".getBuf());
}

FATFS.prototype.close = function () {
	// We first write out directories, this changes the FAT and cluster info.
	fat.root.writedir();

	// Write the FSInfo block and it's backup copy.
	if (this.type == 32) {
		var bs = "\x52\x52\x61\x41"; // FSInfo signature.
		bs += rep(480);
		bs += "\x72\x72\x41\x61"; // Second FSInfo signature.
		bs += dword(this.clusters_free); // Free cluster count.
		bs += dword(this.clusters_next); // Next free cluster hint.
		bs += rep(12);
		bs += "\x00\x00\x55\xAA"; // Sector signature.
		this.sseek(1);
		write(this.f, bs.getBuf());
		if (this.boot_backup != 0) {
			this.sseek(this.boot_backup + 1);
			write(this.f, bs.getBuf());
		}
	}

	// Write the FATs.
	for (var i = 0; i < this.fat_copies; ++i) {
		this.sseek(this.reserved + this.fat_sz * i);
		if (this.type == 12) {
			for (var e = 0; e < this.clusters; e += 2) {
				var h = this.fat[e + 1] & 0x0FFF, l = this.fat[e] & 0x0FFF;
				var ent = byte(l) + byte((l >> 8) | (h << 4)) + byte(h >> 4);
				write(this.f, ent.getBuf());
			}
		} else if (this.type == 16) {
			for (var e = 0; e < this.clusters; ++e)
				write(this.f, word(this.fat[e]).getBuf());
		} else {
			for (var e = 0; e < this.clusters; ++e)
				write(this.f, dword(this.fat[e]).getBuf());
		}
	}

	// KTHXBaii!
	(this.f.close() == 0) || err("error while closing output file");
};

// Seek to sector index.
FATFS.prototype.sseek = function (sector, off = 0) {
	if (this.f.seek(this.offset + sector * this.sector_sz + off,
	    std.SEEK_SET) != 0)
		err("failed to seek");
};

// Seek to cluster.
FATFS.prototype.cseek = function (cluster, off = 0) {
	this.sseek(this.start_off + this.cluster_sz * (cluster - 2), off);
};

// Return next cluster in chain or allocate a new one.
FATFS.prototype.alloc = function (prev) {
	if (prev != 0 && this.fat[prev] <= this.cluster_max)
		return this.fat[prev];
	(this.clusters_free > 0) || err("filesystem full");
	while (this.fat[this.clusters_next])
		++this.clusters_next;
	this.fat[this.clusters_next] = 0x0FFFFFFF;
	if (prev != 0)
		this.fat[prev] = this.clusters_next;
	--this.clusters_free;
	return this.clusters_next++;
};

// Make a cluster chain starting at c a certain length (in clusters).
FATFS.prototype.truncate = function (c, size) {
	var ret = (size > 0) ? ((c == 0) ? c = this.alloc(0) : c) : 0;
	while (--size > 0)
		c = this.alloc(c);
	var last = c;
	while (this.fat[c] <= this.cluster_max) {
		var t = this.fat[c];
		this.fat[c] = (c == last) ? 0x0FFFFFFF : 0;
		c = t;
	}
	return ret;
};

function FATFile(parent, name, opts = {}) {
	this.name = name;
	this.size = opts.size || 0;
	this.flags = opts.flags || (1 << 5);
	this.cluster = 0;
	this.pos = 0;
	this.cluster_pos = 0;
	this.isroot = !(parent instanceof FATDir);
	this.fs = parent.fs;
	parent.children && parent.children.push(this);
	(!this.isroot || this.fs.type == 32) && this.truncate(this.size);
}

// Go to next cluster of file, creating it if necessary.
FATFile.prototype.next = function () {
	if (this.cluster_pos == 0)
		this.cluster_pos = this.cluster || this.fs.alloc(0);
	else this.cluster_pos = this.fs.alloc(this.cluster_pos);
};

FATFile.prototype.seek = function (pos) {
	var cl = Math.ceil(pos / this.fs.cluster_bsz);
	if (pos < this.pos)
		this.cluster_pos = 0;
	else cl -= Math.ceil(this.pos / this.fs.cluster_bsz)
	while (cl-- > 0)
		this.next();
	this.pos = pos;
};

FATFile.prototype.truncate = function (size) {
	var clc = Math.ceil(size / this.fs.cluster_bsz);
	this.cluster = this.fs.truncate(this.cluster, clc);
	this.size = size;
	if (this.pos > this.size)
		this.seek(this.size);
};

FATFile.prototype.write = function (buf) {
	if (this.size < this.pos + buf.byteLength)
		this.truncate(this.pos + buf.byteLength);
	while (buf.byteLength > 0) {
		var wrsz = this.fs.cluster_bsz - this.pos % this.fs.cluster_bsz;
		wrsz = (wrsz > buf.byteLength) ? buf.byteLength : wrsz;
		if (this.pos % this.fs.cluster_bsz == 0)
			this.next();
		this.fs.cseek(this.cluster_pos, this.pos % this.fs.cluster_bsz);
		(this.fs.f.write(buf, 0, wrsz) == wrsz) || err("failed to write");
		buf = buf.slice(wrsz);
		this.pos += wrsz;
		this.size = (this.size < this.pos) ? this.pos : this.size;
	}
};

function FATDir(parent, name, opts = {}) {
	Object.assign(opts, { flags: 1 << 4 });
	Object.assign(this, FATFile.prototype);
	FATFile.call(this, parent, name, opts);
	this.parent = parent;
	this.children = [];
};

FATDir.prototype.getcontent = function () {
	var content = "";
	var names = [], dfns = [];
	if (!this.isroot) {
		var pc = this.parent.isroot ? 0 : this.parent.cluster;
		content += ".          \x10" + rep(8) + word(this.cluster >> 16);
		content += word(0) + word(0) + word(this.cluster) + dword(0);
		content += "..         \x10" + rep(8) + word(pc >> 16);
		content += word(0) + word(0) + word(pc) + dword(0);
	}
	var dfnsum = (dfn) => {
		var sum = 0;
		for (var i = 0; i < 11; ++i)
			sum = ((sum & 0xFF) >> 1) + ((sum & 1) << 7) + dfn.charCodeAt(i);
		return sum;
	};
	var lfnpart = (str, start, end) => {
		var ret = "";
		str = str + "\0";
		for (var i = start; i < end; ++i)
			ret += word((str.length > i) ? str.charCodeAt(i) : 0xFFFF);
		return ret;
	};
	for (var c in this.children) {
		var file = this.children[c];
		var ent = "";

		// Make sure the filename is valid.
		names[file.name] && err(`duplicate filename '${file.name}'`);
		names[file.name] = true;
		var val = /[^A-Z0-9\u007F-\uFFFF\$%'-_@~`!\(\)\{\}\^#&\+,;=\[\]]/i;
		if (file.name.match(val))
			err(`invalid character in filename '${file.name}'`);

		// Figure out the short filename.
		var dfn = file.name.toUpperCase();
		var dfn1 = dfn = dfn.replace(/ /g, "").replace(/^\.*/, "");
		dfn = dfn.replace(/[^A-Z0-9\x7F-\xFF\$%'-_@~`!\(\)\{\}\^#&]/g, "_");
		var dfn_lossy = dfn != dfn1;
		var dfnm = dfn.match(/^([^\.]{0,8})(.*?)(\.([^\.]{0,3}))?([^\.]*)$/);
		dfn = dfnm[1].padEnd(8, " ") + (dfnm[4] || "").padEnd(3, " ");
		// The dfnm[2] and [5] tell us if the name doesn't fit the 8.3 format.
		if (dfnm[2] || dfnm[5] || dfns[dfn] || dfn_lossy) { // Add ~n tail.
			for (var i = 1; i < 999999; ++i) {
				var n = `~${i}`;
				dfn = dfn.substring(0, 8 - n.length) + n + dfn.substring(8);
				if (!dfns[dfn])
					break;
			}
			(i < 999999) || err("what the actual fork?");
		}
		dfns[dfn] = true;

		// Create VFAT LFN entries.
		if (file.name.length > 255)
			err(`filename too long '${file.name}'`);
		var lfns = Math.ceil(file.name.length / 13);
		for (var i = lfns; i > 0; --i) {
			var name = file.name.substring(13 * (i - 1));
			ent += byte(i | ((i == lfns) ? (1 << 6) : 0));
			ent += lfnpart(name, 0, 5);
			ent += "\x0F\x00";
			ent += byte(dfnsum(dfn));
			ent += lfnpart(name, 5, 11);
			ent += word(0);
			ent += lfnpart(name, 11, 13);
		}

		// Add the actual directory entry.
		ent += dfn;
		ent += byte(file.flags);
		ent += rep(8);
		ent += word(file.cluster >> 16);
		ent += word(0x666); // Time. It's 666 for a bug in grub 2.06 prevents 0.
		ent += word(0x666); // Date.
		ent += word(file.cluster);
		ent += dword((file.flags & (1 << 4)) ? 0 : file.size);
		content += ent;
	}
	return content;
};

FATDir.prototype.writedir = function () {
	// Allocate space already, we need this to know our first cluster.
	if (!this.isroot || this.fs.type == 32)
		this.truncate(this.getcontent().length);

	// Write subdirectories.
	for (var c in this.children) {
		if (this.children[c] instanceof FATDir)
			this.children[c].writedir();
	}

	// Write directory contents.
	var cont = this.getcontent(); // Again because clusters may have changed.
	if (this.isroot && this.fs.type != 32) {
		if (cont.length > this.fs.root_sz * 32)
			err("too many files in root directory");
		cont += rep(this.fs.root_sz * 32 - cont.length);
		this.fs.sseek(this.fs.reserved + this.fs.fat_sz * this.fs.fat_copies);
		write(this.fs.f, cont.getBuf());
	} else {
		cont += rep(this.fs.cluster_bsz - cont.length % this.fs.cluster_bsz);
		this.seek(0);
		this.write(cont.getBuf());
	}
};

FATDir.prototype.addtree = function (path) {
	var chksys = (r, msg) => { (r[1] == 0) || err(msg); return r[0]; };
	var d = chksys(os.readdir(path), `can't read directory '${path}'`);
	var dc = [];
	d = d.sort();

	// First create all the directory entries, so we can know their size.
	for (var i in d) {
		if (d[i] == "." | d[i] == "..")
			continue;
		var ent = { fpath: `${path}/${d[i]}` };
		ent.st = chksys(os.lstat(ent.fpath), `can't stat '${ent.fpath}'`)
		if (ent.st.mode & os.S_IFDIR)
			ent.file = new FATDir(this, d[i]);
		else ent.file = new FATFile(this, d[i]);
		dc.push(ent);
	}

	// Now allocate space for the directories, so we get 0 fragmentation.
	if (!this.isroot || this.fs.type == 32)
		this.truncate(this.getcontent().length);

	// Only then process file contents and subdirectories.
	for (var i in dc) {
		if (dc[i].st.mode & os.S_IFDIR) {
			dc[i].file.addtree(dc[i].fpath);
		} else {
			var fin = open(dc[i].fpath, os.O_RDONLY);
			var buf = new ArrayBuffer(4096);
			var size = dc[i].st.size;
			while (size > 0) {
				var psz = Math.min(size, buf.byteLength);
				var l = fin.read(buf, 0, psz);
				(l >= 0) || err(`error reading fille '${dc[i].fpath}'`);
				dc[i].file.write(buf.slice(0, l));
				size -= l;
			}
			fin.close();
		}
	}
}

// Parse command line arguments.
var progname = scriptArgs[0];
var opts = {};
scriptArgs = scriptArgs.slice(1);
for (var i = 0; i < scriptArgs.length; ++i) {
	if (scriptArgs[i] == "--") {
		scriptArgs.splice(i, 1);
		break;
	}
	if (scriptArgs[i].charAt(0) != "-")
		continue;
	var arg = scriptArgs.splice(i, 1)[0];
	var numarg = (min, max) => {
		var ret = Number(scriptArgs.splice(i, 1));
		if (!Number.isInteger(ret) || ret < min || ret > max)
			err(`${arg} requires integer argument >= ${min} and <= ${max}`);
		return ret;
	};
	switch (arg) {
		case "-a": // Align root dir, fat and data by # sectors.
			opts.align = numarg(1, 256); // 256 is an arbitrary limit.
			break;
		case "-b": // Bytes per sector.
			opts.sector_sz = numarg(512, 4096);
			break;
		case "-c": // Sectors per cluster.
			opts.cluster_sz = numarg(1, 255);
			break;
		case "-f": // Number of FAT copies.
			opts.fat_copies = numarg(1, 255);
			break;
		case "-r": // Root directory entries.
			opts.root_sz = numarg(1, 0xFFFF);
			break;
		case "-e": // Reserve extra sectors.
			opts.reserve_extra = numarg(0, 1024);
			break;
		case "-o": // Offset start of filesystem (in sectors).
			opts.offset = numarg(0, 0xFFFFFFFF);
			break;
		case "-n": // Don't truncate output file after opening.
			opts.notrunc = true;
			break;
		default: err(`unknown argument '${arg}'`);
	}
	--i;
}

var size = 0;
if (scriptArgs.length != 3 || !Number.isInteger(size = Number(scriptArgs[1])))
	err(`usage: ${progname} [options] <file> <size> <directory>`);
var fat = new FATFS(scriptArgs[0], Object.assign(opts, { sectors: size }));
fat.root.addtree(scriptArgs[2]);
fat.close();
print(`Created FAT${fat.type} filesystem.`);
print(`  ${fat.clusters - fat.clusters_free} / ${fat.clusters} clusters used`);
print(`  ${fat.cluster_sz} sectors per cluster`);
print(`  ${fat.sector_sz} bytes per sector`);
print(`  ${fat.reserved} reserved sectors`);
print(`  ${fat.fat_sz} sectors per FAT`);
print(`  ${fat.fat_copies} FAT copies`);
print(`  aligned to ${fat.align} sectors`);
(fat.type != 32) && print(`  ${fat.root_sz} root directory entries`);
