#!/bin/sh
set -e

img="boot.img"
dir_efi="efi"
dir_root="target"
dir_overlay="overlay"
dir_tmproot="tmproot"

# All sizes in sectors.
size=4194304 # TODO first digit was 4 but i sized up for pkgsrc
start=2048
start_efi=$start
size_efi=20480
start_root=$(dc -e "$start $size_efi + p")
size_root=$(dc -e "$size $start - $size_efi - 33 - p")
uuid_root="35E116E6-943B-BD4C-940C-2BA3D3751788"

./chroot "$dir_root" . /bin/sh -c "grub-mkimage -O x86_64-efi -o bootx64.efi\
 -p \(hd0,gpt1\)/grub part_msdos part_gpt fat exfat ext2 xfs ntfs ntfscomp\
 iso9660 btrfs blocklist cat cmp echo eval halt normal chain linux video\
 video_fb videoinfo videotest efi_gop font configfile"
mkdir -p "$dir_efi/efi/boot"
mv "$dir_root/bootx64.efi" "$dir_efi/efi/boot"
truncate "$img" -s $((size / 2))K

mkdir -p "$dir_efi/grub"
cat > "$dir_efi/grub/grub.cfg" << ENDGRB
set default=0
set timeout=5

menuentry "MatLinux" {
	linux (hd0,gpt2)/boot/bzImage root=PARTUUID=${uuid_root} ro
}

menuentry "MatLinux (Debug Mode)" {
	linux (hd0,gpt2)/boot/bzImage earlycon=efifb earlyprintk=serial,keep debug root=PARTUUID=${uuid_root} ro
}
ENDGRB

sfdisk "$img" << ENDPT
label: gpt
label-id: 62E99396-6DAF-4449-AB26-334ED13DDDEA
device: img
unit: sectors
first-lba: ${start}
last-lba: $(dc -e "$size 34 - p")
sector-size: 512

img1 : start=${start_efi}, size=${size_efi},\
 type=C12A7328-F81F-11D2-BA4B-00A0C93EC93B,\
 uuid=18C68CDB-5C82-6B4A-9068-BD79E0DAC923
img2 : start=${start_root}, size=${size_root},\
 type=0FC63DAF-8483-4772-8E79-3D69D8477DE4, uuid=${uuid_root}
ENDPT
./mkfat.mjs -n -o $start_efi "$img" $size_efi "$dir_efi"
rm -rf "$dir_tmproot"
mkdir "$dir_tmproot"
cp -a "$dir_root"/. "$dir_tmproot"
rm -rf "$dir_tmproot"/tools # TODO this be nasty
rmdir "$dir_tmproot"/var/db/dhcpcd # TODO, perhaps move this to build step
cp -a "$dir_overlay"/. "$dir_tmproot"
./e2fsprogs/sbin/mke2fs -F -t ext4 -d "$dir_tmproot" -E offset=$(dc -e "$start_root 512 * p"),root_owner=0:0,files_owner=0:0 "$img" $(dc -e "$size_root 2 / p")
rm -rf "$dir_tmproot"
