#include <sys/reboot.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
	int force = 0;
	int type = RB_AUTOBOOT;
	
	{ /* Parse command line. */
		int i, j;
		for (i = 1; i < argc; ++i) for (j = 0; argv[i][j] != 0; ++j) {
			switch (argv[i][j]) {
				case '-':
					break;
				case 'f':
					++force;
					break;
				case 'h':
					type = RB_HALT_SYSTEM;
					break;
				case 'p':
					type = RB_POWER_OFF;
					break;
				/*case 's':
					type = RB_SW_SUSPEND;
					break;*/
				case 'r':
					type = RB_AUTOBOOT;
					break;
				default:
					fprintf(stderr, "usage: %s [-f] [-h|-p|-s|-r]\n"
						"\t-f  force (don't go through init)\n"
						"\t-h  halt\n"
						"\t-p  poweroff\n"
						//"\t-s  suspend\n"
						"\t-r  reboot\n", argv[0]);
					return 1;
			}
		}
	}
	
	if (force) {
		sync();
		reboot(type);
		perror(argv[0]);
		return 1;
	}
	
	// TODO stop perp, kill all processes
	// TODO remount everything as RO
	// TODO show message when system halted
	return 0;
}
