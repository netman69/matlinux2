#!/usr/bin/env qjs
import * as os from "os";
import * as std from "std";

/* About MatLinux
 * 
 * MatLinux is a minimalistic linux distro, or rather some scripts to build
 *  a bootable linux image or chroot.
 * 
 * Some future goals:
 * - Do have an installer and install boot disk.
 *   - Start by asking keyboard layout in layout-agnostic way.
 *   - Have all the documentation needed at hand on install boot disk.
 *   - Include documentation about how to connect to wifi.
 *   - Do not push a particular file system / layout / swap.
 * 
 * Features / selling points:
 * - Minimal but self-hosting base system.
 * - Intended to be easily maintained by a single relatively lazy person.
 * - GCC as the compiler.
 * - Musl libc.
 * - Mksh as the default shell.
 * - Grub2 bootloader (EFI only).
 * - GNU coreutils and util-linux for base system.
 * - Build complete image without requiring root user on the host system.
 * - Everything entirely buildable outside of source directories.
 * - Entire base system can be built with only static binaries.
 * - Basic install should straightforwardly provide documentation on how to
 *     connect to a network, install more software.
 * 
 * My main annoyance with many existing distributions is they either dump you
 *   into a shell and you have to go googling on another machine to figure out
 *   how to use it, or they force KDE or Gnome upon you. Also things are always
 *   broken and too convoluted.
 */

/* Dependencies to get started:
 * - a shell
 * - coreutils/busybox (mkdir, rm, ln, chmod, touch)
 * - glibc or musl
 * - quickjs
 * - curl (or pre-downloaded sources)
 * - tar (even if no sources need extracting)
 * - gzip, lzip, bzip2, xz (or pre-extracted sources)
 * - gnu make
 * - binutils
 * - gcc (with g++ and libstdcxx)
 * - diffutils
 * - findutils
 * - grep, sed, awk
 * - dc for mkimg script (e2fsprogs is now patched and built by us)
 *     also more... i should recheck
 * 
 * For self-hosting this means:
 *   s0: all
 *   s1: all
 *   s2: tar, diffutils, findutils
 *     if no sources, also: curl, gzip, lzip, bzip2, xz
 * 
 * TODO
 *   copy resolv.conf for chroot i guess (but why? if want to curl for matpkgs for example perhaps...)
 *   make all necessary initial files (see dirs target):
 *     http://www.linuxfromscratch.org/lfs/view/stable/chapter07/createfiles.html
 *   e2fsutils etc, maybe cfdisk, for install - apparently util-linux has cfdisk
 *   static build
 *   sbin and usr/sbin in path
 *   chown everything to root:root in chroot (done but see below)
 *     but not proc and dev - why not? i don't remember
 *     check out env -i part here http://www.linuxfromscratch.org/lfs/view/stable/chapter07/chroot.html
 *     currently mkimg.sh uses patched mke2fs we build
 *   note grep can use pcre if it exists
 *   strip everything
 *   script to remove stuff like the target/tools directory
 *   remember cp -a/-rp exists for copying with timestamps and attributes
 *   have to copy /etc/localtime to chroot to get correct time in it
 *   check if wifi works, do we have firmware etc?
 *   ntp client, like chrony
 *   document the caupdate script (downloads a bunch of certificates and puts them somewhere)
 *   these may not be necessary, but idk, if we need NSS for example maybe they are
 *     https://www.linuxfromscratch.org/blfs/view/8.1/postlfs/cacerts.html
 *     https://github.com/djlucas/make-ca
 *   update and upgrade mechanisms
 *   maybe statically include wireless_tools patch
 *   ping, traceroute, etc
 *   probly want netcat too
 *   brctl
 *   lspci, lsusb
 *   more ttys
 *   make gpm also build static binaries
 * 
 *   optional software wanted:
 *     testdisk, tmux, ssh, fuse+sshfs, a hex editor
 * 
 * remember to activate services have to set sticky bit
 * shutdown mechanism (poweroff, halt, reboot)
 *   ignore signals so don't get killed if the controlling terminal dies
 *   stop all services
 *   wait, or not?
 *   kill what doesn't shut down
 *   remount all filesystems as RO or unmount them
 *   sync disks
 *   power off/reboot/whatever
 * pkgsrc expects pwd, mkdir, etc to be in /bin instead of /usr/bin
 *   can't copy gcc there tho, then it breaks (found out by trying to copy all)
 * e2fsprogs needs /tmp to be big enough to build (1mb is too small, 64mb was enough)
 * consider we need to be able to make something like systemrescuecd+install combo and a proper desktop targetted variation of init scripts etc
 * maybe do have a default password so ssh doesn't become a giant too easy hole (automatic login still works)
 * make ctrl-L somehow work to clear screen
 * 
 * to get login follow LFS instructions on creating passwd and group
 *    then run the convert stuff in shadow setup instructions
 * 
 * qemu-system-x86_64 -bios ~/OVMF.fd -drive format=raw,file=boot.img -m 1024 --enable-kvm
 * 
 * todo check how to out of source do the mkimg.sh etc
 * 
 * **********************************************
 *  should we make our own package/build system?
 * **********************************************
 *  - what's the advantages?
 *    * updates are easier without leaving trash from old versions of stuff
 *        but how to keep versions of stuff simple?
 *    * we can manage dependencies (but how to do that and keep it easy and simple?)
 *    * optional things would be easier to deal with
 *  - how can we keep it simple?
 *  - do we want a binary distribution format?
 *  - what should a package have/do?
 *    * build time dependencies
 *    * runtime dependencies
 *    * installed files
 *    * download sources
 *    * build sources
 *    * make package with the files and pre+post (un)install scripts
 *    * store version, dependencies, uninstall etc at install so can be uninstalled/upgrade without the OG package
 *  - should we just use pkgsrc for all things we can?
 */

var path_work = os.getcwd()[0];
var path_script = os.realpath(scriptArgs[0])[0].match(/.*\//);
var path_distfiles = "distfiles";
var path_src = "src";
var path_build = "build";
var path_root = "target";
var path_current = "";

var target = "x86_64-pc-linux-musl";
var host = `x86_64-pc-linux-${getlibc()}`;
var sysroot = path_full(path_root);
var ischroot = false;

var flags_configure = [ "-C" ];
var flags_make = [ `-j${ncpus()}` ];
var flags_make_install = [ flags_make ];

var url_gnu_mirror = "https://mirror.cyberbits.eu/gnu";
var url_gnu = (o) => `${url_gnu_mirror}/${o.name}/${o.name}-${o.ver}${o.type}`;

var env = { FORCE_UNSAFE_CONFIGURE: "1" };

var build_static = false;
var flags_configure_static = build_static ? [ "LDFLAGS=-static" ] : [];
var flags_make_static = build_static ? "LDFLAGS=-all-static" : [];
var flags_make_static2 = build_static ? "LDFLAGS=--static" : [];

var pkgs = {
	e2fsprogs: {
		name: "e2fsprogs", ver: "1.46.4", type: ".tar.gz",
		url: (o) => `https://git.kernel.org/pub/scm/fs/ext2/e2fsprogs.git/snapshot/${o.name}-${o.ver}${o.type}`,
		patch: () => {
			// Patch adds an argument to set owner of files added with -d.
			exec([ "patch", "-p1", "-i", `${path_script}/e2fsprogs.patch` ]);
		}
	},
	binutils: { name: "binutils", ver: "2.37", type: ".tar.gz", url: url_gnu },
	gmp: { name: "gmp", ver: "6.2.1", type: ".tar.lz", url: url_gnu },
	mpfr: { name: "mpfr", ver: "4.1.0", type: ".tar.gz", url: url_gnu },
	mpc: { name: "mpc", ver: "1.2.1", type: ".tar.gz", url: url_gnu },
	isl: {
		name: "isl", ver: "0.24", type: ".tar.gz",
		url: (o) => `https://libisl.sourceforge.io/${o.name}-${o.ver}${o.type}`
	},
	gcc: {
		name: "gcc", ver: "11.2.0", type: ".tar.gz",
		url: (o) => `${url_gnu_mirror}/${o.name}/${o.name}-${o.ver}/${o.name}-${o.ver}${o.type}`,
		patch: () => {
			exec([ "sed", "-e", "/m64=/s/lib64/lib/", "-i.orig", "gcc/config/i386/t-linux64" ]);
			exec([ "ln", "-sf", `../${pkg_dir(pkgs.gmp)}`, "gmp" ]);
			exec([ "ln", "-sf", `../${pkg_dir(pkgs.mpfr)}`, "mpfr" ]);
			exec([ "ln", "-sf", `../${pkg_dir(pkgs.mpc)}`, "mpc" ]);
			exec([ "ln", "-sf", `../${pkg_dir(pkgs.isl)}`, "isl" ]);
		}
	},
	musl: {
		name: "musl", ver: "1.2.2", type: ".tar.gz",
		url: (o) => `https://musl.libc.org/releases/${o.name}-${o.ver}${o.type}`
	},
	mksh: {
		name: "mksh", ver: "R59c", type: ".tar.gz",
		url: (o) => `http://www.mirbsd.org/MirOS/dist/mir/${o.name}/${o.name}-${o.ver}.tgz`,
		dir: (o) => `${o.name}`
	},
	coreutils: { name: "coreutils", ver: "9.0", type: ".tar.gz", url: url_gnu },
	sed: { name: "sed", ver: "4.8", type: ".tar.gz", url: url_gnu },
	grep: { name: "grep", ver: "3.7", type: ".tar.gz", url: url_gnu },
	gawk: { name: "gawk", ver: "5.1.1", type: ".tar.gz", url: url_gnu },
	make: { name: "make", ver: "4.3", type: ".tar.gz", url: url_gnu },
	quickjs: {
		name: "quickjs", ver: "2021-03-27", type: ".tar.xz",
		url: (o) => `https://bellard.org/quickjs/${o.name}-${o.ver}${o.type}`
	},
	libtool: { name: "libtool", ver: "2.4.6", type: ".tar.gz", url: url_gnu },
	diffutils: { name: "diffutils", ver: "3.8", type: ".tar.xz", url: url_gnu },
	findutils: { name: "findutils", ver: "4.8.0", type: ".tar.xz", url: url_gnu },
	tar: { name: "tar", ver: "1.34", type: ".tar.gz", url: url_gnu },
	gzip: { name: "gzip", ver: "1.11", type: ".tar.gz", url: url_gnu },
	lzip: {
		name: "lzip", ver: "1.22", type: ".tar.gz",
		url: (o) => `http://download.savannah.gnu.org/releases/${o.name}/${o.name}-${o.ver}${o.type}`
	},
	xz: {
		name: "xz", ver: "5.2.5", type: ".tar.gz",
		url: (o) => `https://downloads.sourceforge.net/project/lzmautils/${o.name}-${o.ver}${o.type}`
	},
	bzip2: {
		name: "bzip2", ver: "1.0.8", type: ".tar.gz",
		url: (o) => `ftp://sourceware.org/pub/${o.name}/${o.name}-${o.ver}${o.type}`
	},
	patch: { name: "patch", ver: "2.7.6", type: ".tar.gz", url: url_gnu },
	libressl: {
		name: "libressl", ver: "3.4.2", type: ".tar.gz",
		url: (o) => `https://ftp.openbsd.org/pub/OpenBSD/LibreSSL/${o.name}-${o.ver}${o.type}`
	},
	bison: { name: "bison", ver: "3.8.2", type: ".tar.gz", url: url_gnu },
	m4: { name: "m4", ver: "1.4.19", type: ".tar.gz", url: url_gnu },
	flex: {
		name: "flex", ver: "2.6.4", type: ".tar.gz",
		url: (o) => `https://github.com/westes/${o.name}/releases/download/v${o.ver}/${o.name}-${o.ver}${o.type}`
	},
	zlib: {
		name: "zlib", ver: "1.2.11", type: ".tar.gz",
		url: (o) => `https://zlib.net/${o.name}-${o.ver}${o.type}`
	},
	elfutils: {
		name: "elfutils", ver: "0.186", type: ".tar.bz2",
		url: (o) => `ftp://sourceware.org/pub/${o.name}/${o.ver}/${o.name}-${o.ver}${o.type}`
	},
	ed: { name: "ed", ver: "1.17", type: ".tar.lz", url: url_gnu },
	perl: {
		name: "perl", ver: "5.34.0", type: ".tar.gz",
		url: (o) => `https://www.cpan.org/src/5.0/${o.name}-${o.ver}${o.type}`
	},
	texinfo: {
		name: "texinfo", ver: "6.8", type: ".tar.gz", url: url_gnu,
		patch: () => {
			// The patch fixes GPM build, it's upstream so >6.8 won't need it.
			exec([ "patch", "-p1", "-i", `${path_script}/texinfo.patch` ]);
		}
	},
	bc: { name: "bc", ver: "1.07.1", type: ".tar.gz", url: url_gnu },
	rsync: {
		name: "rsync", ver: "3.2.3", type: ".tar.gz",
		url: (o) => `https://download.samba.org/pub/${o.name}/src/${o.name}-${o.ver}${o.type}`
	},
	ncurses: {
		name: "ncurses", ver: "6.3", type: ".tar.gz",
		url: (o) => `https://invisible-mirror.net/archives/${o.name}/${o.name}-${o.ver}${o.type}`
	},
	linux: {
		name: "linux", ver: "5.15.6", type: ".tar.gz",
		url: (o) => `https://cdn.kernel.org/pub/${o.name}/kernel/v5.x/${o.name}-${o.ver}${o.type}`,
		patch: () => {
			// Patch fixes an include for musl.
			exec([ "patch", "-p1", "-i", `${path_script}/linux.patch` ]);
		}
	},
	util_linux: {
		name: "util-linux", ver: "2.37.2", ver_dir: "2.37", type: ".tar.gz",
		url: (o) => `https://mirrors.edge.kernel.org/pub/linux/utils/${o.name}/v${o.ver_dir}/${o.name}-${o.ver}${o.type}`
	},
	shadow: {
		name: "shadow", ver: "4.9", type: ".tar.gz",
		url: (o) => `https://github.com/shadow-maint/${o.name}/releases/download/v${o.ver}/${o.name}-${o.ver}${o.type}`
	},
	/*python: { // Needs --no-same-owner for tar because stupidity
		name: "python", ver: "3.9.2", rname: "Python", type: ".tar.gz",
		url: (o) => `https://www.python.org/ftp/${o.name}/${o.ver}/${o.rname}-${o.ver}.tgz`,
		dir: (o) => `${o.rname}-${o.ver}`
	},*/
	grub: {
		name: "grub", ver: "2.06", type: ".tar.gz", url: url_gnu,
		patch: () => {
			exec([ "sed", "s/gold-version/& -R .note.gnu.property/", "-i", "Makefile.in", "grub-core/Makefile.in" ]);
		}
	},
	/*argp-standalone: {
		name: "argp-standalone", ver: "1.3", type: ".tar.gz",
		url: (o) => `http://www.lysator.liu.se/~nisse/misc/${o.name}-${o.ver}${o.type}`
	},*/
	/*musl-fts: {
		name: "musl-fts", ver: "1.2.7", type: ".tar.gz",
		url: (o) => `https://github.com/void-linux/${o.name}/archive/v${o.ver}${o.type}`
	},*/
	/*musl-obstack: {
		name: "musl-obstack", ver: "1.2.2", type: ".tar.gz",
		url: (o) => `https://github.com/void-linux/${o.name}/archive/v${o.ver}${o.type}`
	},*/
	perp: {
		name: "perp", ver: "2.07", type: ".tar.gz",
		url: (o) => `http://b0llix.net/${o.name}/distfiles/${o.name}-${o.ver}${o.type}`,
		patch: () => {
			// Patch needed for static build.
			exec([ "patch", "-p1", "-i", `${path_script}/perp.patch` ]);
		}
	},
	curl: {
		name: "curl", ver: "7.80.0", type: ".tar.gz",
		url: (o) => `https://curl.se/download/${o.name}-${o.ver}${o.type}`
	},
	iproute2: {
		name: "iproute2", ver: "5.11.0", type: ".tar.gz",
		url: (o) => `https://github.com/shemminger/${o.name}/archive/v${o.ver}${o.type}`
	},
	dhcpcd: {
		name: "dhcpcd", ver: "9.4.1", type: ".tar.xz",
		url: (o) => `https://roy.marples.name/downloads/${o.name}/${o.name}-${o.ver}${o.type}`
	},
	/*ninja: { // Not used, wtf build system claiming to be minimal requires CMake or python to build?!
		name: "ninja", ver: "1.10.2", type: ".tar.gz",
		url: (o) => `https://github.com/ninja-build/${o.name}/archive/refs/tags/v${o.ver}${o.type}`
	},*/
	/*iputils: { // Contains ping, traceroute, etc. Not used because dumb build system.
		name: "iputils", ver: "20210722", type: ".tar.gz",
		url: (o) => `https://github.com/${o.name}/${o.name}/archive/refs/tags/${o.ver}${o.type}`
	},*/
	/* Only needed for wireless. */
	/*wireless_tools: { // Deprecated by iw.
		name: "wireless_tools", ver: "29", type: ".tar.gz",
		url: (o) => `https://hewlettpackard.github.io/wireless-tools/${o.name}.${o.ver}${o.type}`,
		dir: (o) => `${o.name}.${o.ver}`,
		patch: () => {
			exec([ "curl", "-f", "-L", "-O", "https://www.linuxfromscratch.org/patches/blfs/svn/wireless_tools-29-fix_iwlist_scanning-1.patch" ]);
			exec([ "patch", "-Np1", "-i", "wireless_tools-29-fix_iwlist_scanning-1.patch" ]);
		}
	},*/
	libnl: { // Needed by iw and wpa_supplicant.
		name: "libnl", ver: "3.5.0", type: ".tar.gz",
		url: (o) => `https://github.com/thom311/libnl/releases/download/libnl3_5_0/${o.name}-${o.ver}${o.type}`
	},
	iw: {
		name: "iw", ver: "5.9", type: ".tar.xz",
		url: (o) => `https://www.kernel.org/pub/software/network/${o.name}/${o.name}-${o.ver}${o.type}`
	},
	readline: { // Needed by wpa_supplicant.
		name: "readline", ver: "8.1", type: ".tar.gz",
		url: (o) => `ftp://ftp.cwru.edu/pub/bash/${o.name}-${o.ver}${o.type}`
	},
	wpa_supplicant: {
		name: "wpa_supplicant", ver: "2.9", type: ".tar.gz",
		url: (o) => `https://w1.fi/releases/${o.name}-${o.ver}${o.type}`
	},
	pkg_config: {
		name: "pkg-config", ver: "0.29.2", type: ".tar.gz",
		url: (o) => `https://pkgconfig.freedesktop.org/releases/${o.name}-${o.ver}${o.type}`
	},
	autoconf: { name: "autoconf", ver: "2.71", type: ".tar.gz", url: url_gnu }, // Needed for procps, elinks, maybe more.
	automake: { name: "automake", ver: "1.16.5", type: ".tar.gz", url: url_gnu }, // Needed for procps, elinks, maybe more.
	gettext: { name: "gettext", ver: "0.21", type: ".tar.gz", url: url_gnu }, // Needed for procps.
	procps: {
		name: "procps", ver: "v3.3.17", type: ".tar.gz",
		url: (o) => `https://gitlab.com/${o.name}-ng/${o.name}/-/archive/${o.ver}/${o.name}-${o.ver}${o.type}`,
		patch: () => {
			// Patch to fix building with musl.
			exec([ "patch", "-p1", "-i", `${path_script}/procps.patch` ]);
		}
	},
	/* Entirely optional stuff. */
	gpm: { // Gives mouse support for nano and elinks, maybe other things.
		name: "gpm", ver: "1.20.7", type: ".tar.bz2",
		url: (o) => `https://www.nico.schottelius.org/software/${o.name}/archives/${o.name}-${o.ver}${o.type}`,
		patch: () => {
			// Patch to fix building with musl.
			exec([ "patch", "-p1", "-i", `${path_script}/gpm.patch` ]);
		}
	},
	less: {
		name: "less", ver: "590", type: ".tar.gz",
		url: (o) => `https://www.greenwoodsoftware.com/${o.name}/${o.name}-${o.ver}${o.type}`
	},
	mandoc: {
		name: "mandoc", ver: "1.14.6", type: ".tar.gz",
		url: (o) => `https://mandoc.bsd.lv/snapshots/${o.name}-${o.ver}${o.type}`
	},
	nano: {
		name: "nano", ver: "5.9", type: ".tar.gz",
		url: (o) => `https:/www.nano-editor.org/dist/v5/${o.name}-${o.ver}${o.type}`
	},
	file: {
		name: "file", ver: "5.39", type: ".tar.gz",
		url: (o) => `ftp://ftp.astron.com/pub/${o.name}/${o.name}-${o.ver}${o.type}`
	},
	tree: {
		name: "tree", ver: "1.8.0", type: ".tar.gz",
		url: (o) => `http://mama.indstate.edu/users/ice/${o.name}/src/${o.name}-${o.ver}.tgz`
	},
	ddrescue: { name: "ddrescue", ver: "1.25", type: ".tar.lz", url: url_gnu },
	ncdu: {
		name: "ncdu", ver: "1.16", type: ".tar.gz",
		url: (o) => `https://dev.yorhel.nl/download/${o.name}-${o.ver}${o.type}`
	},
	elinks: {
		name: "elinks", ver: "0.14.3", type: ".tar.gz",
		url: (o) => `https://github.com/rkd77/${o.name}/archive/refs/tags/v${o.ver}${o.type}`
	},
	htop: {
		name: "htop", ver: "3.1.2", type: ".tar.gz",
		url: (o) => `https://github.com/${o.name}-dev/${o.name}/archive/refs/tags/${o.ver}${o.type}`
	}
};

/* Stage 0 - Build toolchain to target chroot. */
var targets_s0 = {
	chroot: {
		name: "chroot",
		build: (tgt) => {
			print("Building chroot binary...");
			exec([
				"gcc", "-o", `${path_work}/${tgt.name}`,
				`${path_script}/${tgt.name}.c`
			]);
		}
	},
	dirs: {
		name: "dirs",
		build: (tgt) => {
			print("Creating target directories...");
			var root = path_full(path_root);
			mkdir(root);
			mkdir(`${root}/dev`);
			mkdir(`${root}/proc`);
			mkdir(`${root}/sys`);
			mkdir(`${root}/tmp`);
			mkdir(`${root}/mnt`);
			mkdir(`${root}/root`);
		}
	},
	e2fsprogs: {
		pkg: pkgs.e2fsprogs,
		flags_configure: [
			flags_configure,
			`--prefix=${path_work}/e2fsprogs`,
			"--disable-nls",
		],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	binutils: {
		pkg: pkgs.binutils,
		flags_configure: [
			flags_configure,
			`--target=${target}`,
			`--program-prefix=${target}-`,
			`--prefix=${sysroot}/tools`,
			`--with-sysroot=${sysroot}`,
			"--disable-nls",
			"--disable-werror"
		],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ `${tgt_spath(tgt)}/configure`, tgt.flags_configure ], tgt.env);
			// Skipping configure-host causes all sorts of subtle issues.
			exec([ "make", "configure-host", tgt.flags_make ], tgt.env);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	gcc: {
		pkg: pkgs.gcc,
		pkg_extra: [ pkgs.gmp, pkgs.mpfr, pkgs.mpc, pkgs.isl ],
		flags_configure: [
			flags_configure,
			`--target=${target}`,
			`--prefix=${sysroot}/tools`,
			`--with-sysroot=${sysroot}`,
			"--enable-languages=c,c++",
			"--disable-multilib",
			"--disable-nls",
			"--without-headers",
			"--disable-initfini-array",
			"--disable-bootstrap",
			"--disable-shared",
			"--disable-lto",
			"--disable-libsanitizer",
			"--disable-libmudflap",
			"--disable-threads",
			"--disable-libatomic",
			"--disable-libgomp",
			"--disable-libquadmath",
			"--disable-libssp",
			"--disable-libvtv",
			"--disable-libstdcxx",
			"--with-newlib"
		],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
};

/* Stage 1 - Build toolchain usable inside chroot. */
var targets_s1 = {
	musl: {
		pkg: pkgs.musl,
		flags_configure: [ `--build=${host}`, `--host=${target}`, "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: [ flags_make_install, `DESTDIR=${sysroot}` ],
		build: (tgt) => {
			tgt_build_autoconf(tgt);
			mkdir(`${sysroot}/bin`);
			exec([ "ln", "-sf", "/lib/ld-musl-x86_64.so.1", `${sysroot}/bin/ldd` ]);
		}
	},
	mksh: {
		pkg: pkgs.mksh,
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ "chmod", "+x", `${tgt_spath(tgt)}/Build.sh` ]);
			exec([ `${tgt_spath(tgt)}/Build.sh` ], {
				CC: `${sysroot}/tools/bin/${target}-gcc`,
				LDSTATIC: build_static ? "-static" : []
			});
			mkdir(`${sysroot}/bin`);
			mkdir(`${sysroot}/usr/share/man/man1`);
			mkdir(`${sysroot}/usr/share/doc/mksh/examples`);
			exec([ "install", "-c", "-s", "-m", "555", "mksh", `${sysroot}/bin/mksh` ]);
			exec([ "ln", "-sf", "/bin/mksh", `${sysroot}/bin/sh` ]);
			exec([ "install", "-c", "-m", "444", "FAQ.htm", `${sysroot}/usr/share/doc/mksh/` ]);
			enter(tgt_spath(tgt));
			exec([ "install", "-c", "-m", "444", "dot.mkshrc", `${sysroot}/usr/share/doc/mksh/examples/` ]);
			exec([ "install", "-c", "-m", "444", "lksh.1", "mksh.1", `${sysroot}/usr/share/man/man1/` ]);
		}
	},
	coreutils: {
		pkg: pkgs.coreutils,
		flags_configure: [
			flags_configure, flags_configure_static,
			`--build=${host}`, `--host=${target}`, "--prefix=/usr",
			"--disable-nls", "--enable-install-program=hostname"
		],
		flags_make: flags_make,
		flags_make_install: [ flags_make_install, `DESTDIR=${sysroot}` ],
		build: tgt_build_autoconf
	},
	sed: {
		pkg: pkgs.sed,
		flags_configure: [
			flags_configure, flags_configure_static,
			`--build=${host}`, `--host=${target}`, "--prefix=/usr", "--disable-nls"
		],
		flags_make: flags_make,
		flags_make_install: [ flags_make_install, `DESTDIR=${sysroot}` ],
		build: tgt_build_autoconf
	},
	grep: {
		pkg: pkgs.grep,
		flags_configure: [
			flags_configure, flags_configure_static,
			`--build=${host}`, `--host=${target}`, "--prefix=/usr", "--disable-nls"
		],
		flags_make: flags_make,
		flags_make_install: [ flags_make_install, `DESTDIR=${sysroot}` ],
		build: tgt_build_autoconf
	},
	gawk: {
		pkg: pkgs.gawk,
		flags_configure: [
			flags_configure, flags_configure_static,
			`--build=${host}`, `--host=${target}`, "--prefix=/usr", "--disable-nls"
		],
		flags_make: flags_make,
		flags_make_install: [ flags_make_install, `DESTDIR=${sysroot}` ],
		build: tgt_build_autoconf
	},
	make: {
		pkg: pkgs.make,
		flags_configure: [
			flags_configure, flags_configure_static,
			`--build=${host}`, `--host=${target}`, "--prefix=/usr", "--disable-nls"
		],
		flags_make: flags_make,
		flags_make_install: [ flags_make_install, `DESTDIR=${sysroot}` ],
		build: tgt_build_autoconf
	},
	binutils: {
		pkg: pkgs.binutils,
		flags_configure: [
			flags_configure, flags_configure_static,
			`--build=${host}`, `--host=${target}`, "--prefix=/usr", "--disable-werror", "--with-debuginfod=no"
		],
		flags_make: flags_make,
		flags_make_install: [ flags_make_install, `DESTDIR=${sysroot}` ],
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ `${tgt_spath(tgt)}/configure`, tgt.flags_configure ], tgt.env);
			// Skipping configure-host causes all sorts of subtle issues.
			exec([ "make", "configure-host", tgt.flags_make ], tgt.env);
			exec([ "make", tgt.flags_make, flags_make_static ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	libstdcxx: {
		// We need this to build gcc.
		name: "libstdcxx",
		pkg: pkgs.gcc,
		pkg_extra: [ pkgs.gmp, pkgs.mpfr, pkgs.mpc, pkgs.isl ],
		flags_configure: [
			flags_configure,
			`--build=${host}`, `--host=${target}`, "--prefix=/usr",
			"--disable-multilib",
			"--disable-nls",
			"--disable-libstdcxx-pch",
			//`--with-gxx-include-dir=/usr/include/c++/${pkgs.gcc.ver}`
		],
		spath: `${pkg_dir(pkgs.gcc)}/libstdc++-v3`,
		flags_make: flags_make,
		flags_make_install: [ flags_make_install, `DESTDIR=${sysroot}` ],
		build: tgt_build_autoconf
	},
	gcc: {
		pkg: pkgs.gcc,
		pkg_extra: [ pkgs.gmp, pkgs.mpfr, pkgs.mpc, pkgs.isl ],
		flags_configure: [
			flags_configure, build_static ? [
				"--with-boot-ldflags=-static",
				"--with-stage1-ldflags=-static"
			] : [],
			`--build=${host}`, `--host=${target}`, `--target=${target}`, "--prefix=/usr",
			`--with-build-sysroot=${sysroot}`,
			`CXXFLAGS=-isystem ${sysroot}/usr/include/c++/${pkgs.gcc.ver} -isystem ${sysroot}/usr/include/c++/${pkgs.gcc.ver}/${target}`,
			"--enable-languages=c,c++",
			"--disable-multilib",
			"--disable-nls",
			"--disable-initfini-array",
			"--disable-bootstrap",
			"--disable-lto",
			"--disable-libsanitizer", // This needs kernel headers.
			"--disable-libmudflap",
			"--disable-libatomic",
			"--disable-libgomp",
			"--disable-libquadmath",
			"--disable-libssp",
			"--disable-libstdcxx",
			"--disable-shared",
			"--with-newlib"
		],
		flags_make: flags_make,
		flags_make_install: [ flags_make_install, `DESTDIR=${sysroot}` ],
		build: (tgt) => {
			tgt_build_autoconf(tgt);
			exec([ "ln", "-sf", "/usr/bin/gcc", `${sysroot}/usr/bin/cc` ]);
		}
	},
	quickjs: {
		pkg: pkgs.quickjs,
		flags_make: [
			flags_make, `CROSS_PREFIX=${target}-`, "CONFIG_LTO=",
			build_static ? "LDFLAGS=-static" : []
		],
		flags_make_install: [
			flags_make_install, `CROSS_PREFIX=${target}-`, "CONFIG_LTO=", "prefix=/usr", `DESTDIR=${sysroot}`
		],
		build: (tgt) => {
			tgt_build_begin(tgt)
			exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	}
};

/* Stage 2 - Built in chroot. */
var targets_s2 = {
	m4: { // Needed by: libtool, bison, flex
		pkg: pkgs.m4,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	libtool: {
		pkg: pkgs.libtool,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	diffutils: { // Needed to build binutils and gcc.
		pkg: pkgs.diffutils,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	findutils: { // Needed to build gcc/libstdcxx.
		pkg: pkgs.findutils,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	tar: { // Needed to build gcc/libstdcxx.
		pkg: pkgs.tar,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls", "--without-xattrs", "--without-posix-acls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	gzip: {
		pkg: pkgs.gzip,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	lzip: {
		pkg: pkgs.lzip,
		flags_configure: [ flags_configure_static, "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	xz: {
		pkg: pkgs.xz,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: [ flags_make, flags_make_static ],
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	bzip2: {
		pkg: pkgs.bzip2,
		flags_make: [ flags_make, "CFLAGS=-fPIC", build_static ? "LDFLAGS=-static" : [] ],
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt)
			exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "-f", "Makefile-libbz2_so", tgt.flags_make ], tgt.env);
			exec([ "make", "install", "PREFIX=/usr", tgt.flags_make_install ], tgt.env);
			exec([ "make", "-f", "Makefile-libbz2_so", "install", "PREFIX=/usr", tgt.flags_make_install ], tgt.env);
		}
	},
	patch: {
		pkg: pkgs.patch,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	libressl: {
		pkg: pkgs.libressl,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr" ],
		flags_make: [ flags_make, flags_make_static ],
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	/* Below probably not needed for non-booting chroot. */
	bison: { // Needed to build linux.
		pkg: pkgs.bison,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	flex: { // Needed to build linux.
		pkg: pkgs.flex,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: [ flags_make, flags_make_static ],
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	zlib: { // Needed to build libelf, rsync, linux.
		pkg: pkgs.zlib,
		flags_configure: [ "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	elfutils: { // We only build libelf from this.
		pkg: pkgs.elfutils,
		env: { ac_cv_search_argp_parse: "1", ac_cv_search_fts_close: "1", ac_cv_search__obstack_free: "1" }, // Trick configure to ignore some dependencies.
		flags_configure: [ flags_configure, "--prefix=/usr", "--disable-libdebuginfod", "--disable-debuginfod" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ `${tgt_spath(tgt)}/configure`, tgt.flags_configure ], tgt.env);
			os.chdir("libelf");
			exec([ "touch", "error.h" ]);
			mkdir("sys");
			exec([ "touch", "sys/cdefs.h" ]);
			exec([ "touch", "../lib/libeu.a" ]);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	ed: { // Needed to build bc.
		pkg: pkgs.ed,
		flags_configure: [ flags_configure_static, "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	perl: { // Needed to build texinfo, coreutils.
		pkg: pkgs.perl,
		flags_configure: [ "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([
				`${tgt_spath(tgt)}/Configure`, "-des", "-Dprefix=/usr", "-Dvendorprefix=/usr", "-Dmksymlinks",
				build_static ? [ "-Aldflags=-static", "-Uusedl" ] : []
			], tgt.env);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	texinfo: { // Needed to build bc.
		pkg: pkgs.texinfo,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	bc: { // Needed to build linux.
		pkg: pkgs.bc,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	rsync: { // We need zlib first, needed to build linux.
		pkg: pkgs.rsync,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-xxhash", "--disable-zstd", "--disable-lz4", "--without-included-zlib" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		env: { rsync_cv_have_fallocate: "no" }, /* So that building twice works. */
		build: tgt_build_autoconf
	},
	ncurses: {
		pkg: pkgs.ncurses,
		flags_configure: [ flags_configure, flags_configure_static, "--with-libtool", "--with-shared" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	linux: {
		pkg: pkgs.linux,
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ "cp", `${path_script}/linux.config`, ".config" ], tgt.env);
			exec([ "make", "-f", `${tgt_spath(tgt)}/Makefile`, "olddefconfig", "O=.", tgt.flags_make ], tgt.env);
			exec([ "make", "headers_install", "INSTALL_HDR_PATH=/usr", tgt.flags_make ], tgt.env);
			exec([ "make", "bzImage", tgt.flags_make ], tgt.env);
			mkdir("/boot");
			exec([ "cp", "arch/x86_64/boot/bzImage", "/boot" ]);
		}
	},
	util_linux: { // Needs linux before compile.
		pkg: pkgs.util_linux,
		flags_configure: [
			flags_configure, flags_configure_static, build_static ? "--enable-static-programs" : [],
			//"--prefix=/usr", "--disable-nls", "--disable-use-tty-group", "--disable-makeinstall-chown", "--disable-chfn-chsh", "--disable-login", "--disable-nologin", "--disable-su", "--disable-setpriv", "--disable-runuser", "--disable-pylibmount", "--without-python", "--without-systemd", "--without-systemdsystemunitdir"
			"--prefix=/usr", "--disable-nls", "--disable-use-tty-group", "--disable-makeinstall-chown", "--disable-chfn-chsh", "--disable-login", "--disable-nologin", "--disable-su", "--disable-setpriv", "--disable-runuser", "--disable-pylibmount", "--without-python", "--without-systemd", "--without-systemdsystemunitdir"
		],
		flags_make: [ flags_make, flags_make_static ],
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	shadow: {
		pkg: pkgs.shadow,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls", "--enable-subordinate-ids=no" ],
		flags_make: [ flags_make, flags_make_static2 ],
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
			exec([ "./configure", tgt.flags_configure ], tgt.env);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	/*python: { // Needed to build grub (or is it?).
		pkg: pkgs.python,
		flags_configure: [ flags_configure, build_static ? [ "--prefix=/usr", "--disable-shared", "LDFLAGS=-static", "CFLAGS=-static", "CPPFLAGS=-static" ] : [] ],
		flags_make: [ flags_make ],
		flags_make_install: [ flags_make_install ],
		build: tgt_build_autoconf
	},*/
	grub: {
		pkg: pkgs.grub,
		flags_configure: [ /*flags_configure,*/ flags_configure_static, "--prefix=/usr", "--disable-werror", "--disable-nls", "--with-platform=efi" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		env: { PYTHON: "true" }, // Dirty hack to escape from a snake.
		build: tgt_build_autoconf
	},
	perp: {
		pkg: pkgs.perp,
		flags_make: [ flags_make, build_static ? "TLDFLAGS=-static" : [] ],
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	curl: {
		pkg: pkgs.curl,
		flags_configure: [ flags_configure, "--prefix=/usr", "--with-ca-bundle=/etc/cacert.pem", "--with-openssl" ],
		flags_make: [ flags_make, flags_make_static ],
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	iproute2: {
		pkg: pkgs.iproute2,
		flags_make: [ flags_make, flags_make_static2 ],
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	dhcpcd: {
		pkg: pkgs.dhcpcd,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls", "--disable-privsep" ],
		flags_make: [ flags_make, flags_make_static2 ],
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
			exec([ "./configure", tgt.flags_configure ], tgt.env);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	/*iputils: { // Not used because dumb build system.
		pkg: pkgs.iputils,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},*/
	/*wireless_tools: { // Deprecated by iw.
		pkg: pkgs.wireless_tools,
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		env: { PREFIX: "/usr" },
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},*/
	libnl: { // Needed for iw and wpa_supplicant.
		pkg: pkgs.libnl,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr" ],
		flags_make: [ flags_make, flags_make_static ],
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	iw: {
		pkg: pkgs.iw,
		flags_make: [ flags_make, flags_make_static2 ],
		flags_make_install: flags_make_install,
		env: { NO_PKG_CONFIG: true, CFLAGS: "-O2 -I/usr/include/libnl3 -DCONFIG_LIBNL30", LIBS: "-lnl-3 -lnl-genl-3", NLLIBNAME: "libnl-3.5" },
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	readline: { // Needed by wpa_supplicant.
		pkg: pkgs.readline,
		flags_configure: [ flags_configure, /*flags_configure_static,*/ "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	wpa_supplicant: {
		pkg: pkgs.wpa_supplicant,
		flags_make: [ flags_make, flags_make_static2 ],
		env: { BINDIR: "/usr/sbin", LIBDIR: "/usr/lib" },
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
			enter(path_current + "/wpa_supplicant");
			exec([ "cp", path_script + "/wpa_config", ".config" ]);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "install", "-m744", "wpa_cli", "/usr/sbin" ], tgt.env);
			exec([ "install", "-m744", "wpa_passphrase", "/usr/sbin" ], tgt.env);
			exec([ "install", "-m744", "wpa_supplicant", "/usr/sbin" ], tgt.env);
			exec([ "install", "-m644", "doc/docbook/wpa_supplicant.conf.5", "/usr/share/man/man5/" ], tgt.env);
			exec([ "install", "-m644", "doc/docbook/wpa_cli.8", "/usr/share/man/man8/" ], tgt.env);
			exec([ "install", "-m644", "doc/docbook/wpa_passphrase.8", "/usr/share/man/man8/" ], tgt.env);
			exec([ "install", "-m644", "doc/docbook/wpa_supplicant.8", "/usr/share/man/man8/" ], tgt.env);
		}
	},
	autoconf: {
		pkg: pkgs.autoconf,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	automake: {
		pkg: pkgs.automake,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	gettext: {
		pkg: pkgs.gettext,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: [ flags_make, flags_make_static ],
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	pkg_config: {
		pkg: pkgs.pkg_config,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls", "--with-internal-glib" ],
		flags_make: [ flags_make, flags_make_static ],
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	procps: {
		pkg: pkgs.procps,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr" ],
		flags_make: [ flags_make, flags_make_static ],
		flags_make_install: flags_make_install,
		build: tgt_build_autogen
	},
	/* Optional rebuild. */
	/*libstdcxx: {
		name: "libstdcxx",
		pkg: pkgs.gcc,
		pkg_extra: [ pkgs.gmp, pkgs.mpfr, pkgs.mpc, pkgs.isl ],
		flags_configure: [
			flags_configure, "--prefix=/usr",
			`--host=${target}`, "--disable-multilib", "--disable-nls"
		],
		spath: `${pkg_dir(pkgs.gcc)}/libstdc++-v3`,
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},*/
	/*binutils: {
		pkg: pkgs.binutils,
		flags_configure: [
			flags_configure, flags_configure_static,
			"--prefix=/usr", "--disable-nls", "--disable-werror"
		],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt);
			exec([ `${tgt_spath(tgt)}/configure`, tgt.flags_configure ], tgt.env);
			// Skipping configure-host causes all sorts of subtle issues.
			exec([ "make", "configure-host", tgt.flags_make ], tgt.env);
			exec([ "make", tgt.flags_make, build_static ? "LDFLAGS=-all-static" : [] ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	gcc: {
		pkg: pkgs.gcc,
		pkg_extra: [ pkgs.gmp, pkgs.mpfr, pkgs.mpc, pkgs.isl ],
		flags_configure: [
			flags_configure, build_static ? [
				"--with-boot-ldflags=-static",
				"--with-stage1-ldflags=-static"
			] : [], [
			"--prefix=/usr",
			"--enable-languages=c,c++",
			"--disable-multilib",
			"--disable-nls",
			"--disable-initfini-array",
			"--disable-bootstrap",
			"--disable-shared",
			"--disable-lto",
			"--disable-libsanitizer", // This needs kernel headers.
			"--disable-libmudflap",
			"--disable-libatomic",
			"--disable-libgomp",
			"--disable-libquadmath",
			"--disable-libssp",
			"--disable-libstdcxx"
		],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_autoconf(tgt);
			exec([ "ln", "-sf", "/usr/bin/gcc", `${sysroot}/usr/bin/cc` ]);
		}
	},*/
	/* Not needed. */
	/*argp-standalone: { // Elfutil "needs" this we work around that.
		pkg: pkgs.argp-standalone,
		flags_configure: [ flags_configure, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},*/
	/*musl-fts: { // Elfutil "needs" this we work around that.
		pkg: pkgs.musl-fts,
		flags_configure: [ flags_configure, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},*/
	/*musl-obstack: { // Elfutil "needs" this we work around that.
		pkg: pkgs.musl-obstack,
		flags_configure: [ flags_configure, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},*/
	/* Entirely optional things. */
	gpm: {
		pkg: pkgs.gpm,
		flags_configure: [ flags_configure, "--prefix=/usr", "--without-curses" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autogen
	},
	less: {
		pkg: pkgs.less,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	mandoc: {
		pkg: pkgs.mandoc,
		flags_configure: [ flags_configure, flags_configure_static ],
		flags_make: [ flags_make, flags_make_static2 ],
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt)
			exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
			exec([ "cp", `${path_script}/mandoc.conf`, "configure.local" ]);
			exec([ "./configure", tgt.flags_configure ], tgt.env);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	nano: {
		pkg: pkgs.nano,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	tree: {
		pkg: pkgs.tree,
		flags_make: [ flags_make, flags_make_static2 ],
		flags_make_install: flags_make_install,
		build: (tgt) => {
			tgt_build_begin(tgt)
			exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
			exec([ "make", tgt.flags_make ], tgt.env);
			exec([ "make", "install", tgt.flags_make_install ], tgt.env);
		}
	},
	file: {
		pkg: pkgs.file,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr" ],
		flags_make: [ flags_make, flags_make_static ],
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	ddrescue: {
		pkg: pkgs.ddrescue,
		flags_configure: [ flags_configure_static, "--prefix=/usr" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	ncdu: {
		pkg: pkgs.ncdu,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autoconf
	},
	elinks: {
		pkg: pkgs.elinks,
		flags_configure: [ flags_configure_static, "--prefix=/usr", "--disable-nls" ],
		flags_make: [ flags_make, flags_make_static2 ],
		flags_make_install: flags_make_install,
		build: tgt_build_autogen
	},
	htop: {
		pkg: pkgs.htop,
		flags_configure: [ flags_configure, flags_configure_static, "--prefix=/usr", "--disable-nls", "--disable-unicode" ],
		flags_make: flags_make,
		flags_make_install: flags_make_install,
		build: tgt_build_autogen
	},
};

var targets = {
	s0: {
		targets: targets_s0,
		env: { PATH: std.getenv("PATH") }
	},
	s1: {
		targets: targets_s1,
		env: { PATH: `${sysroot}/tools/bin:${std.getenv("PATH")}` }
	},
	s2: {
		targets: targets_s2,
		env: { PATH: "/bin:/sbin:/usr/bin:/usr/sbin" },
		chroot: true
	}
};

function ncpus() {
	var f = std.open("/proc/cpuinfo", "r");
	var s = f.readAsString();
	f.close();
	return (s.match(/^processor/gm) || []).length;
}

function getlibc() {
	var f = std.popen("ldd --version 2>&1", "r");
	var s = f.readAsString();
	f.close();
	return s.match(/^musl/) ? "musl" : "gnu";
}

function path_full(path) {
	return (path.charAt(0) == '/') ? path : `${path_work}/${path}`;
}

function mkdir(path) {
	if (os.exec(["mkdir", "-p", path]) != 0)
		throw `error: cannot create directory ${path}`;
}

function enter(path) {
	if (path == path_current)
		return;
	path_current = path;
	print(`Entering "${path}".`);
	var fp = path_full(path);
	mkdir(fp);
	if (os.chdir(fp) != 0)
		throw `can't enter ${path}`;
}

function flatten(arr) {
	var ret = [];
	for (var i in arr) {
		if (Array.isArray(arr[i]))
			ret = ret.concat(flatten(arr[i]));
		else ret.push(arr[i]);
	}
	return ret;
}

function exec(line, e) {
	if (os.exec(flatten(line), { env: e || env }) != 0)
		throw `error: command returned non-zero status: ${line}`;
}

function trycall(f, thisArg, args) {
	return (f && f.constructor === Function && f.apply(thisArg, args)) || f;
}

function pkg_dir(pkg) {
	return trycall(pkg.dir, pkg, [ pkg ]) || `${pkg.name}-${pkg.ver}`;
}

function pkg_download(pkg, force = false) {
	var file = `${pkg.name}-${pkg.ver}${pkg.type}`;
	enter(path_distfiles);
	/* Check if we have a tarball. */
	if (!force && os.stat(file)[0]) { /* TODO check integrety of file */
		print(`Found ${file}.`);
		return;
	}
	/* Download the tarball. */
	print(`Downloading ${file}...`);
	var url = trycall(pkg.url, pkg, [ pkg ]);
	if (os.exec([ "curl", "-f", "-L", "-o", file, url ]) != 0)
		throw `error: failed to download ${url}`;
};

function pkg_extract(pkg, force = false) {
	var file = `${pkg.name}-${pkg.ver}${pkg.type}`;
	var path = path_full(`${path_distfiles}/${file}`);
	/* Check if source directory exists. */
	if (!force && os.stat(path_full(`${path_src}/${pkg_dir(pkg)}`))[0]) { /* TODO check integrety of dir contents */
		print(`Found ${pkg_dir(pkg)}.`);
		return;
	}
	/* Download the source if it doesn't exist yet. */
	pkg_download(pkg);
	enter(path_src);
	/* Remove existing source directory if it exists. */
	if (os.exec(["rm", "-rf", pkg.dir]) != 0)
		throw `error: failed to delete existing sources at ${path}`;
	/* Extract the sources. */
	print(`Extracting ${pkg.name}-${pkg.ver}...`);
	if (pkg.type == ".tar.gz") {
		if (os.exec(["tar", "xzf", path]) != 0)
			throw `error: failed to extract ${file}`;
	} else if (pkg.type == ".tar.lz") {
		if (os.exec(["tar", "x", "--lzip", "-f", path]) != 0)
			throw `error: failed to extract ${file}`;
	} else if (pkg.type == ".tar.bz2") {
		if (os.exec(["tar", "xjf", path]) != 0)
			throw `error: failed to extract ${file}`;
	} else if (pkg.type == ".tar.xz") {
		if (os.exec(["tar", "xJf", path]) != 0)
			throw `error: failed to extract ${file}`;
	} else if (pkg.type != "nodownload")
		throw `error: unknown type for ${file}`;
	if (pkg.patch) {
		enter(`${path_src}/${pkg_dir(pkg)}`);
		print(`Patching ${pkg.name}-${pkg.ver}...`);
		pkg.patch.call(pkg, pkg);
	}
};

function tgt_name(tgt) {
	return tgt.name || tgt.pkg.name;
}

function tgt_spath(tgt) {
	return path_full(`${path_src}/` + (trycall(tgt.spath, tgt, [ tgt ]) || pkg_dir(tgt.pkg)));
}

function tgt_bpath(tgt) {
	return path_full(`${path_build}/${tgt.stage}_${tgt_name(tgt)}`);
}

/* Things to do before every build. */
function tgt_build_begin(tgt) {
	if (tgt.pkg_extra) {
		for (var i in tgt.pkg_extra)
			pkg_extract(tgt.pkg_extra[i]);
	}
	pkg_extract(tgt.pkg);
	enter(tgt_bpath(tgt));
	print(`Building ${tgt_name(tgt)}...`);
}

function tgt_build_autoconf(tgt) {
	tgt_build_begin(tgt);
	exec([ `${tgt_spath(tgt)}/configure` ].concat(tgt.flags_configure), tgt.env);
	exec([ "make", tgt.flags_make ], tgt.env);
	exec([ "make", "install", tgt.flags_make_install ], tgt.env);
}

function tgt_build_autogen(tgt) {
	tgt_build_begin(tgt)
	exec([ "cp", "-a", `${tgt_spath(tgt)}/.`, "." ]);
	exec([ "./autogen.sh" ], tgt.env);
	exec([ "./configure", tgt.flags_configure ], tgt.env);
	exec([ "make", tgt.flags_make ], tgt.env);
	exec([ "make", "install", tgt.flags_make_install ], tgt.env);
}

function tgt_build(s, t) {
	var tgt = targets[s].targets[t];
	tgt.stage = s;
	tgt.env = Object.assign(env, targets[s].env, tgt.env);
	if (targets[s].chroot) {
		if (!ischroot) {
			var sname = scriptArgs[0].replace(/.*\//, '');
			if (os.realpath(path_script)[0] != os.realpath(path_work)[0])
				exec([ "cp", os.realpath(scriptArgs[0])[0], path_work ]);
			exec([
				`${path_work}/chroot`, path_full(path_root), `${path_work}`, "/bin/sh", "-c",
				`cd /mnt && ./${sname} -c ${s} ${t}`
			]);
		} else tgt.build.call(tgt, tgt);
	} else tgt.build.call(tgt, tgt);
}

for (var i = 0; i < scriptArgs.length; ++i) {
	if (scriptArgs[i] == "-c") { // -c argument means don't chroot
		ischroot = true;
		scriptArgs.splice(i, 1);
		--i;
	}
}

for (var i in pkgs)
	pkg_extract(pkgs[i]);

if (scriptArgs.length > 2) {
	tgt_build(scriptArgs[1], scriptArgs[2]);
} else if (scriptArgs.length > 1) {
	for (var t in targets[scriptArgs[1]].targets)
		tgt_build(scriptArgs[1], t);
} else {
	for (var s in targets)
		for (var t in targets[s].targets)
			tgt_build(s, t);
}
