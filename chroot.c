/* taken from https://github.com/sabotage-linux/sabotage */
#define _GNU_SOURCE
#include <sched.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/mount.h>
#include <stdio.h>
#include <limits.h>

#define chk(X) if((X) == -1) { perror(#X); exit(1); }
#define chkn(X) if((X) == NULL) { perror(#X); exit(1); }

int main(int argc, char *argv[]) {
	uid_t uid = getuid();
	uid_t gid = getgid();
	char buf[32], mntpath[PATH_MAX];
	int fd;

	if (argc < 4) {
		fprintf(stderr, "usage: %s <path> <mount> <cmd>\n", argv[0]);
		return 1;
	}

	chk(unshare(CLONE_NEWUSER|CLONE_NEWNS));

	fd = open("/proc/self/uid_map", O_RDWR);
	write(fd, buf, snprintf(buf, sizeof buf, "0 %u 1\n", uid));
	close(fd);

	fd = open("/proc/self/setgroups", O_RDWR);
	write(fd, buf, snprintf(buf, sizeof buf, "deny\n"));
	close(fd);

	fd = open("/proc/self/gid_map", O_RDWR);
	write(fd, buf, snprintf(buf, sizeof buf, "0 %u 1\n", gid));
	close(fd);

	chkn(realpath(argv[2], mntpath));

	chdir(argv[1]);
	chk(mount("/dev", "./dev", 0, MS_BIND|MS_REC, 0));
	chk(mount("/proc", "./proc", 0, MS_BIND|MS_REC, 0));
	chk(mount("/sys", "./sys", 0, MS_BIND|MS_REC, 0));
	chk(mount("/tmp", "./tmp", 0, MS_BIND|MS_REC, 0));
	chk(mount(mntpath, "./mnt", 0, MS_BIND, 0));

	chk(chroot("."));

	chk(execv(argv[3], argv + 3));
}
